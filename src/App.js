import React, { useEffect, useState } from 'react';
import { Amplify, Auth, Hub } from 'aws-amplify';
import { CognitoHostedUIIdentityProvider } from '@aws-amplify/auth';
import awsconfig from './aws-exports';
import FacebookLogin from 'react-facebook-login';
import { GoogleLogin } from 'react-google-login';
import { gapi } from 'gapi-script';


const clientId = '717324258916-t4p195vd82u7jrshhvpqi7o9tdnlj05o.apps.googleusercontent.com';
Amplify.configure(awsconfig);

function App() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const initClient = () => {
      gapi.client.init({
        clientId: clientId,
        scope: ''
      });
    };
    gapi.load('client:auth2', initClient);
  });

  useEffect(() => {
    const unsubscribe = Hub.listen("auth", ({ payload: { event, data } }) => {
      switch (event) {
        case "signIn":
          setUser(data);
          break;
        case "signOut":
          setUser(null);
      }
    });

    Auth.currentAuthenticatedUser()
      .then(currentUser => {
        console.log("currentUser", currentUser)
        setUser(currentUser)
      })
      .catch(() => console.log("Not signed in"));

    return unsubscribe;
  }, []);

  const requestToServer = (data) => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ data })
    };
    fetch('http://localhost:8080/', requestOptions)
      .then(response => {
        console.log("response", response)
        // response.json() 
      })
  }
  const responseFacebook = (data) => {
    requestToServer(data)

  }

  const onSuccess = (res) => {
    console.log('success:', res);
    requestToServer(res)
  };
  const onFailure = (err) => {
    console.log('failed:', err);
  };
  return (
    <div className="App" style={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      height: '100vh',
    }}>
      <GoogleLogin
        clientId={clientId}
        buttonText="Sign in with Google"
        onSuccess={onSuccess}
        onFailure={onFailure}
        cookiePolicy={'single_host_origin'}
        isSignedIn={true}
      />

      <FacebookLogin
        appId="2320157891484580"
        autoLoad={true}
        fields="name,email"
        // onClick={componentClicked}
        callback={responseFacebook} />



      <button onClick={() => Auth.federatedSignIn()}>Open Hosted UI</button>
      <button onClick={() => Auth.federatedSignIn({ provider: CognitoHostedUIIdentityProvider.Facebook })}>Open Facebook</button>
      <button onClick={() => Auth.federatedSignIn({ provider: CognitoHostedUIIdentityProvider.Google })}>Open Google</button>
      <button onClick={() => Auth.signOut()}>Sign Out</button>
      <div>{user && user.getUsername()}</div>
    </div>
  );
}

export default App;
